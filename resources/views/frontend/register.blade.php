@extends('frontend.app')
@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{ route('register.store') }}" class="form-horizontal " method="post">
                @csrf
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">register user</h5>
                    </div>

                    <div class="panel-body">

                        <div class="box-body">
                            <div class="form-group">
                                <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="add name ">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" value="{{old('email')}}" name="email" placeholder="add email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" value="{{old('password')}}" name="password" placeholder=" add password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>


                        </div>

                    </div>

                    <div class="text-right" style="padding-bottom: 20px; padding-right: 20px;">
                        <input type="submit" class="btn btn-primary"
                               value="Register"/>
                    </div>

                </div>


            </form>
        </div>
        <!-- /basic layout -->

    </div>
    </div>
@endsection