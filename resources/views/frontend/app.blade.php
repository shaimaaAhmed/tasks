<html>
<head>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="{{route('index')}}">Home</a>
        @if (Auth::guest())
            <div class="login-container">
                <a href="{{route('login')}}" class="btn">Login</a>
                <a href="{{route('register')}}" class="btn">Register</a>
            </div>
        @else
            <a href="{{route('task')}}">Task</a>
            <div class="login-container " style="margin-right: 80px">
                <a href="{{route('logout')}}" class="btn">Logout</a>
                <img src="{{asset('image/user.png')}}" width="40" height="40">
                <label class="ml-5">{{auth('web')->user()->name}}</label>
            </div>
        @endif
    </div>
</header>
@yield('content')

<script type="text/javascript" src="{{asset('frontend/js/script.js')}}"></script>
@yield('scripts')

</body>
</html>