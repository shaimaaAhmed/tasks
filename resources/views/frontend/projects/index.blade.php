@extends('frontend.app')

@section('content')


    <!-- Basic datatable -->
    <div class="panel panel-flat" style="margin: 20px;">
        <div class="panel-heading">
            <h5 class="panel-title">projects</h5>
        </div>
        <div class="list-icons" style="padding-right: 10px;">
            <a href="{{ route('project.create') }}" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                            class="icon-plus2"></i></b>Add Project</a>
        </div>


        <table class="table datatable-basic" id="categories" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Start project</th>
                <th>End project</th>
                <th>Date added</th>
                <th>Tasks</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($projects as $project)
                <tr id="project-row-{{ $project->id }}">

                    <td>{{ $project->id }}</td>
                    <td> {{ $project->name}}</td>
                    <td> {{ $project->start_time}}</td>
                    <td> {{ $project->end_time}}</td>
                    <td>{{ \Carbon\Carbon::parse($project->created_at)->diffForHumans() }}</td>
                    <td>  <a href="{{route('project.task.index',$project->id)}}"
                             class="btn btn-success" style="margin-bottom: 5px">Tasks</a>
                    </td>
                    <td class="text-center">
                        <a href="{{route('project.edit',$project->id)}}" class="btn btn-primary" style="margin-bottom: 5px">edit</a>
                        <form method="POST" action="{{ route('Project.delete',$project->id) }}">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button type="submit" class="btn btn-xs btn-danger btn-flat show_confirm" data-toggle="tooltip" title='Delete'> <i class="fa fa-trash"> </i>Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop
@section('scripts')
    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}

    <script type="text/javascript">
        $('.show_confirm').click(function(e) {
            if(!confirm('Are you sure you want to delete this?')) {
                e.preventDefault();
            }
        });
    </script>
    @stop
