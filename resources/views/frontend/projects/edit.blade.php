@extends('frontend.app')
@section('content')
        <div class="row" style="margin: 15px;">
            <div class="col-md-12">

                <!-- Basic layout-->
                <form action="{{ route('project.update',$project->id) }}" class="form-horizontal " method="post">
                    @csrf
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Edit project {{$project->name}}</h5>
                        </div>

                        <div class="panel-body">

                            <div class="box-body">
                                <div class="form-group">
                                    <label style="margin-left: 10px">name</label>
                                    <input type="text" class="form-control" value="{{$project->name}}" name="name" placeholder="project name ">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label style="margin-left: 10px">start time</label>
                                    <input type="text" class="form-control" value="{{$project->start_time}}" name="start_time" placeholder="">

                                    @error('start_time')
                                    <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label style="margin-left: 10px">end time</label>
                                    <input type="text" class="form-control" value="{{$project->end_time}}" name="end_time" placeholder=" ">

                                    @error('end_time')
                                    <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>


                            </div>

                        </div>

                        <div class="text-right" style="padding-bottom: 20px; padding-right: 20px;">
                            <input type="submit" class="btn btn-primary"
                                   value="Edit project"/>
                        </div>

                    </div>


                </form>
            </div>
            <!-- /basic layout -->

        </div>
    </div>
    @endsection