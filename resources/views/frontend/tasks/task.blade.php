@extends('frontend.app')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="todolist not-done">
                    <h1>Todos</h1>
                    <div class="list-icons" style="padding-right: 10px; margin-bottom: 10px">
                        <a href="{{ route('project.create') }}" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                                        class="icon-plus2"></i></b>Add Project</a>
                        <a href="{{ route('project.index') }}" class="btn btn-success btn-labeled btn-labeled-right"><b><i
                                        class="icon-plus2"></i></b>Edit & Delete Project</a>
                    </div>

                    <form action="{{route('add.task')}}" method="post">
                        @csrf
                        <div  class="form-group mt-2 mb-2">
                            <select class="form-control" name="project_id" id="project">

                                <option>select project</option>
                                @forelse ($projects as $project)
                                    <option value="{{$project->id}}" >{{$project->name}}</option>
                                @empty
                                    <li>There are no projects </li>
                                @endforelse
                            </select>
                            @error('project_id')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <input type="text" class="form-control " placeholder="Add task" name="name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </form>

                    <button id="checkAll" class="btn btn-success">Mark all as done</button>

                    <hr>
                    <div id="count"></div>
                    <ul id="sortable" class="list-unstyled">

                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="todolist">
                    <h1>Already Done</h1>
                    <ul id="done-items" class="list-unstyled">

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>
        $("#project").change(function (e) {
            e.preventDefault();
            var project_id = $("select#project").val();
            if(project_id){
                $.ajax({
                    url: '{{request()->root()}}/project/'+project_id+'/task',
                    type:'GET',
                    success:function (response)
                    {
                        // console.log(response);
                        if(response.status === 1)
                        {
                            let subCat = $("ul#sortable");
                            let checked = $("ul#done-items");
                            let count = $("div#count");
                            subCat.html('');
                            jQuery.each(response.tasks, function (index, sortable) {
                                if(sortable.status == 'finish'){
                                    checked.append('<li>'+sortable.name+'<button class="remove-item btn btn-default btn-xs pull-right" onclick="myDeleteFunction('+sortable.id+')">'+
                                        '<span class="glyphicon glyphicon-remove"></span></button></li>');
                                }else {
                                    subCat.append('<li class="ui-state-default">'+
                                        '<div class="checkbox">'+
                                        '<label>'+
                                        '<input type="checkbox" value="'+sortable.id+'" id="myCheck" onclick="myFunction()" />'+sortable.name+'</label>' +
                                        '</div></li>');
                                }
                            });
                            count.append('<label>The tasks were completed with proportions:'+response.percentage+'%</label>');

                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        console.log(errorMessage);
                    }
                });
            }
            else{
                $("#sortable").empty();
                $("#sortable").append('<i value=""></i>');
            }
        });


        function myFunction() {
            // Get the checkbox
            var checkBox = document.getElementById("myCheck");
            // If the checkbox is checked, display the output text
            if (checkBox.checked == true) {
                var tast_id = $("#myCheck").val();
                if (tast_id) {
                    // alert(tast_id);
                    $.ajax({
                        url: '{{request()->root()}}/change/task/'+tast_id+'',
                        type: 'POST',
                        data: {_token: '{{ csrf_token() }}'},
                        success:function(response){
                            console.log('change');
                        },
                    });
                }

            } else {
                alert('no checked');
            }
        }

        function myDeleteFunction(id) {
            // e.preventDefault();
            if (id) {
                // console.log(id);
                alert('Are You sure delete Task ');
                $.ajax({
                    url: '{{request()->root()}}/task/delete/'+id+'',
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                    success: function (data) {
                        console.log('deleted');
                    },
                });
            }
            // }
        }

    </script>
@stop
