@extends('frontend.app')

@section('content')


    <!-- Basic datatable -->
    <div class="panel panel-flat" style="margin: 20px;">
        <div class="panel-heading">
            <h5 class="panel-title">Tasks</h5>
        </div>
        <table class="table datatable-basic" id="categories" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>project</th>
                <th>status</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr id="task-row-{{ $task->id }}">

                    <td>{{ $task->id }}</td>
                    <td> {{ $task->name}}</td>
                    <td> {{ ($task->project)->name}}</td>
                    <td> {{ $task->status}}</td>
                    <td>{{ \Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</td>

                    <td class="text-center">
                        <a href="{{ route('task.edit',$task->id) }}" class="btn btn-primary" style="margin-bottom: 5px">edit</a>
                        <form method="POST" action="{{ route('delete',$task->id) }}">
                            @csrf
                            <input name="_method" type="hidden" value="POST">
                            <button type="submit" class="btn btn-xs btn-danger btn-flat show_confirm" data-toggle="tooltip" title='Delete'> <i class="fa fa-trash"> </i>Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop
@section('scripts')
    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}

    <script type="text/javascript">
        $('.show_confirm').click(function(e) {
            if(!confirm('Are you sure you want to delete this?')) {
                e.preventDefault();
            }
        });
    </script>
    @stop
