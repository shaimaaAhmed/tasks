@extends('frontend.app')
@section('content')
        <div class="row" style="margin: 15px;">
            <div class="col-md-12">

                <!-- Basic layout-->
                <form action="{{ route('task.update',$task->id) }}" class="form-horizontal " method="post">
                    @csrf
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Edit task {{$task->name}}</h5>
                        </div>

                        <div class="panel-body">

                            <div class="box-body">
                                <div class="form-group">
                                    <label style="margin-left: 10px">name</label>
                                    <input type="text" class="form-control" value="{{$task->name}}" name="name" placeholder="task name ">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label style="margin-left: 10px">status</label>
                                    <input type="text" class="form-control" value="{{$task->status}}" name="status" placeholder="">

                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>



                        </div>

                        <div class="text-right" style="padding-bottom: 20px; padding-right: 20px;">
                            <input type="submit" class="btn btn-primary"
                                   value="Edit task"/>
                        </div>

                    </div>

                    </div>
                </form>
            </div>
            <!-- /basic layout -->

        </div>
    @endsection