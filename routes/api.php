<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController\AuthController;
use App\Http\Controllers\ApiController\MainController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);

    Route::post('/create/Project', [MainController::class, 'createProject']);
    Route::post('/update/Project/{id}', [MainController::class, 'updateProject']);
    Route::post('/delete/Project/{id}', [MainController::class, 'deleteProject']);
    Route::get('/user-Project', [MainController::class, 'viewProjects']);

    Route::post('/create/task', [MainController::class, 'addTask']);
    Route::post('/update/task/{id}', [MainController::class, 'updateTask']);
    Route::post('/delete/task/{id}', [MainController::class, 'deleteTask']);
    Route::get('/user-tasks', [MainController::class, 'viewAllTasks']);
    Route::get('/view-tasks-Project', [MainController::class, 'viewTasksProject']);
    Route::post('/update/status/task', [MainController::class, 'updateStatusTask']);
});