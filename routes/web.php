<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController\AuthController;
use App\Http\Controllers\FrontController\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/register', [AuthController::class, 'register'])->name('register');
    Route::post('register/store', [AuthController::class, 'registerStore'])->name('register.store');

    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('login/store', [AuthController::class, 'loginStore'])->name('login.store');


    Route::get('/index', [TaskController::class, 'index'])->name('index');

    Route::group(['middleware' => 'web',], function () {
        Route::get('/task', [TaskController::class, 'task'])->name('task');

        Route::get('project/index', [TaskController::class, 'indexProject'])->name('project.index');

        Route::get('project/create', [TaskController::class, 'createProject'])->name('project.create');
        Route::post('project/store', [TaskController::class, 'storeProject'])->name('project.store');

        Route::get('project/tasks/{id}', [TaskController::class, 'projectTasks'])->name('project.task.index');

        Route::get('project/edit/{id}', [TaskController::class, 'editProject'])->name('project.edit');
        Route::post('project/update/{id}', [TaskController::class, 'updateProject'])->name('project.update');

        Route::delete('project/delete/{id}', [TaskController::class, 'deleteProject'])->name('Project.delete');

        Route::get('project/{id}/task', [TaskController::class, 'tasksProject']);
        Route::post('add/task', [TaskController::class, 'addTask'])->name('add.task');
        Route::post('change/task/{id}', [TaskController::class, 'statusTask'])->name('change.status');

        Route::get('task/edit/{id}', [TaskController::class, 'editTask'])->name('task.edit');
        Route::post('task/update/{id}', [TaskController::class, 'updateTask'])->name('task.update');

        Route::post('task/delete/{id}', [TaskController::class, 'deleteTask'])->name('delete');

        Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    });

