<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
//            'end_time' => 'required|date_format:Y-m-d H:i:s',
        ];
    }
        public function messages()
    {
        return [
            'name.required' => 'A name project is required',
            'start_time.required' => 'A start_time message is required',
            'end_time.required' => 'A end_time message is required',
        ];
    }
}
