<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>      'required|string|max:20',
            'email'             =>      'required|email|unique:users,email',
            'password'          =>      'required|alpha_num|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name project is required',
            'email.required' => 'A email message is required',
            'password.required' => 'A password message is required',
        ];
    }
}
