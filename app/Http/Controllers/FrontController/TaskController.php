<?php

namespace App\Http\Controllers\FrontController;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Validator;

class TaskController extends Controller
{
    public function __construct() {
        $this->middleware('auth:web', ['except' => ['index']]);
    }

    public function index()
    {
        return view('frontend.index');
    }
    public function task()
    {
        $auth = auth('web')->user();
        $projects = Project::whereUserId($auth->id)->get();
//        $tasks = Task::get();
        return view('frontend.tasks.task', compact( 'projects'));
    }

    public function indexProject(){
        $auth = auth('web')->user();
        $projects = Project::whereUserId($auth->id)->get();
        return view('frontend.projects.index', compact( 'projects'));
    }

    public function createProject(){
        return view('frontend.projects.create');
    }

    public function storeProject(StoreProjectRequest $request){
       $user_id = auth('web')->user()->id;
        Project::create([
            'name'=> $request->name,
            'start_time'=> $request->start_time,
            'end_time'=> $request->end_time,
            'user_id'=> $user_id,
        ]);
        return redirect()->intended('task')->with(['message' =>'added successfully']);
    }

    public function editProject($id){
        $project = Project::find($id);
        if ($project){
            return view('frontend.projects.edit',compact('project'));
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }

    public function updateProject(StoreProjectRequest $request ,$id){
        $project = Project::find($id);;
        if ($project){
            $request_data = $request->except('_token');
            $project->update($request_data);
            return redirect()->back()->with(['message' =>'updated successfully']);
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }

    public function deleteProject($id){
        $project = Project::find($id);
        if ($project){
            $project->delete();
            return redirect()->back()->with(['message' =>'delete successfully']);
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }

    public function projectTasks($id)
    {
        try {
           $tasks= Task::whereProjectId($id)->get();
            return view('frontend.tasks.index',compact('tasks'));
        }
        catch (\Exception $e) {
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }


    /// =========   Task add edit and delete  / tasks all project
    public function tasksProject($id)
    {
        try {
            $tasks =Task::whereProjectId($id);
            $tasksArr = $tasks->get()->toArray();
            $task_count = $tasks->get()->count();
            $countTasks = $tasks->whereStatus('finish')->get()->count();
            $count = (100/$task_count);
            $percentage = $count * $countTasks;
            return response()->json(['status' => 1, 'message' => 'done', 'tasks' => $tasksArr ,'percentage'=>$percentage]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage(), 'tasks' => []], 500);
        }
    }

    public function addTask(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('task')->withErrors($validator)->withInput();
        }
        $request_data = $request->except(['_token']);
        Task::create($request_data);
        return redirect()->intended('task')->with(['message' =>'added successfully']);

    }

    /// update status task
    public function statusTask($id){
            $task = Task::find($id);
            if ($task){
                $status = $task->status;
                if ($status == 'onProgress'){
                $task->update(['status' => 'finish']);
            }
            else{
                    $task->update(['status' => 'finish']);
            }
                return redirect()->intended('task')->with(['message' =>'update successfully']);
            }
               else{
                return redirect()->back()->with('message', 'IT WORKS!');
            }
    }



    public function editTask($id){
            $task = Task::find($id);
        if ($task){
            return view('frontend.tasks.edit',compact('task'));
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }

    public function updateTask(Request $request ,$id){
        $task = Task::find($id);;
        if ($task){
            $request_data = $request->except('_token');
            $task->update($request_data);
            return redirect()->back()->with(['message' =>'updated successfully']);
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }


    public function deleteTask($id){
//        dd($id);
        $task = Task::find($id);
        if ($task){
            $task->delete();

            return redirect()->back()->with(['message' =>'delete successfully']);
        }
        else{
            return redirect()->back()->with('message', 'IT WORKS!');
        }
    }
}