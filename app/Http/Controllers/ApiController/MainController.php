<?php

namespace App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Validator;

class MainController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }


    public function createProject(StoreProjectRequest $request){
        $auth = auth('api')->user();
        if ($auth){
         $project = Project::create([
            'name'=> $request->name,
            'start_time'=> $request->start_time,
            'end_time'=> $request->end_time,
            'user_id'=>$auth->id,
        ]);
            return response()->json([
                'message' => 'added project successfully',
                'project' => $project
            ], 201);
        }else{
            return response()->json(['message' => 'no user name']);
        }
    }

    public function viewProjects(){

       $auth = auth('api')->user();
       if ($auth){
           $projects = Project::whereUserId($auth->id)->get();
          return response()->json([
            'message' => 'successfully',
            'projects' => $projects
        ], 201);
       }else{
          return response()->json(['message' => 'no user name']);
       }
   }

    public function updateProject(StoreProjectRequest $request ,$id){
        $project = Project::find($id);;
        if ($project){
            $request_data = $request->except('_token');
            $project->update($request_data);
            return response()->json([
                'message' => 'updated successfully',
                'project' => $project
            ], 201);
        }
        else{
            return response()->json(['message' => 'no project name']);
        }
    }

    public function deleteProject($id){
        $project = Project::find($id);
        if ($project){
            $project->delete();
        return response()->json([
            'message' => 'delete successfully',
        ], 201);
        }
        else{
            return response()->json(['message' => 'no project name']);
        }
    }

////////////// tasks add and delete update  and  view Tasks all Project

    public function addTask(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('task')->withErrors($validator)->withInput();
        }
        $request_data = $request->except(['_token']);
         $task = Task::create($request_data);
        return response()->json([
            'message' => 'added task successfully',
            'task' => $task
        ], 201);
    }

    public function viewTasksProject(Request $request){

        $auth = auth('api')->user();
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if ($auth){
            $project = Project::find($request->project_id);
            if ($project){
                $tasks = Task::whereProjectId($request->project_id)->get();
                $countTasks = Task::whereProjectId($request->project_id)->whereStatus('finish')->get()->count();
              $count = (100/$tasks->count());
              $percentage = $count * $countTasks;

                return response()->json([
                    'message' => 'successfully',
                    'tasks' => $tasks,
                    'percentage' => $percentage.'%',
                ], 201);
            }else{
                return response()->json(['message' => 'لا يوجد مشروع بهذا الاسم']);
            }
        }else{
            return response()->json(['message' => 'لا يوجد مستخدم بهذا الاسم']);
        }
    }

    public function viewAllTasks(){

        $auth = auth('api')->user();
        if ($auth){
            $tasks = Task::whereHas('project' , function ($query) use ($auth){
                $query->where('user_id' , $auth->id);
            })->get();
            return response()->json([
                'message' => 'successfully',
                'tasks' => $tasks
            ], 201);
        }else{
            return response()->json(['message' => 'لا يوجد مستخدم بهذا الاسم']);
        }
    }

    public function updateStatusTask(Request $request){

        $auth = auth('api')->user();
        $validator = Validator::make($request->all(), [
            'task_id' => 'required',
            'status' => 'required|in:onProgress,finish',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if ($auth){
            $task = Task::find($request->task_id);
            if ($task){
            $task->update(['status' => $request->status]);
            return response()->json([
                'message' => 'update successfully',
          ]);}else{
                return response()->json(['message' => 'لا يوجد تاسك بهذا الاسم']);
            }
        }else{
            return response()->json(['message' => 'لا يوجد مستخدم بهذا الاسم']);
        }
    }

    public function updateTask(Request $request ,$id){
        $task = Task::find($id);;
        if ($task){
            $request_data = $request->except('_token');
            $task->update($request_data);
            return response()->json([
                'message' => 'updated successfully',
                'task' => $task
            ], 201);
        }
        else{
            return response()->json(['message' => 'no task name']);
        }
    }

    public function deleteTask($id){
        $task = Task::find($id);
        if ($task){
            $task->delete();
            return response()->json([
                'message' => 'delete successfully',
            ], 201);
        }
        else{
            return response()->json(['message' => 'no task name']);
        }
    }
}
