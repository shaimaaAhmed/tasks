<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=1 ;$i <= 6 ; $i++){
        DB::table('tasks')->insert([
            'name' =>$faker->word,
            'project_id' => 1,
        ]);
        }

        $faker = Faker::create();
        for ($i=1 ;$i <= 8 ; $i++){
            DB::table('tasks')->insert([
                'name' =>$faker->word,
                'project_id' => 2,
            ]);
        }
    }
}
